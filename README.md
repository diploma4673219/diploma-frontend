# diploma-frontend

### Local development via Docker
#### Prerequisite
Docker version >= 20.10.22
Docker Compose version >= v2.15.1
```
docker -v
docker compose version
```
#### How to use
```
docker compose up --build
```

Vue app serves on `localhost:8081`. Hot reload enabled.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
