import { StringSchema as YupStringSchema } from 'yup';
import { FORMATS } from '../../services/Files';

declare module 'yup' {
  interface StringSchema {
    staticLink(): YupStringSchema;
  }
  interface StringSchema {
    [FORMATS.IMAGE](): YupStringSchema;
  }
  interface StringSchema {
    [FORMATS.VIDEO](): YupStringSchema;
  }
}
