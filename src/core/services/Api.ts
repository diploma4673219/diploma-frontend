import axios, { AxiosInstance, AxiosResponse, AxiosRequestConfig, AxiosError } from 'axios';
import router from '@/router';
import { ErrorBase } from '../types/base';
import useModalConfirm from '@/components/modal/useModalConfirm';
import { ModalConfirmTypes } from '@/components/modal/types';
import { i18n } from '@/core/services/I18n';

export const AUTH_TOKEN_KEY = 'access_token';
const BASE_API_URL = `${import.meta.env.VITE_APP_API_URL}`;

/**
 ** Pagination
 */
export interface Pagination {
  page: number; // Номер текущей страницы.
  page_size: number; // Сколько элементов на одной странице.
  total_count: number; // Сколько всего элементов.
}

export type PaginationRequest = Omit<Pagination, 'total_count'>;

/**
 ** Success response
 */
export interface ResponseSuccessBody<T = unknown> {
  pagination?: Pagination;
  result: T;
}

/**
 ** Error response
 */
// interface ResponseErrorDescriptionItem {
//   loc: string[];
//   msg: string;
// }

interface ResponseErrorBody {
  detail: string;
}

export const enum ApiErrorNames {
  'API_RESPONSE_ERROR' = 'API Response Error',
}

export class ApiError extends ErrorBase<ApiErrorNames> {
  code: number;
  detail: string;

  constructor(name: ApiErrorNames, statusCode: number, statusText: string) {
    super(name, `${statusCode} ${statusText}`);

    this.detail = statusText;
    this.code = statusCode;
  }
}

export abstract class HttpClient {
  protected readonly instance: AxiosInstance;

  protected constructor(baseURL: string) {
    const config: AxiosRequestConfig = {
      baseURL,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    };

    this.instance = axios.create(config);
    this._initializeRequestInterceptor();
    this._initializeResponseInterceptor();
  }

  private _initializeRequestInterceptor(): void {
    this.instance.interceptors.request.use(this._handleRequest);
  }

  private _initializeResponseInterceptor(): void {
    this.instance.interceptors.response.use(this._handleResponse, this._handleError);
  }

  protected _handleRequest(config: AxiosRequestConfig) {
    const token = localStorage.getItem(AUTH_TOKEN_KEY);

    if (!token) {
      return config;
    }

    return {
      ...config,
      headers: {
        ...config.headers,
        Authorization: `Bearer ${token}`,
      },
    };
  }

  protected _handleResponse({ data }: AxiosResponse) {
    return data;
  }

  protected async _handleError(error: AxiosError<ResponseErrorBody>) {
    const confirmError = useModalConfirm(`${i18n.global.t('error')}!`, `${i18n.global.t('error_save')}!`, {
      onlyConfirm: true,
      clickToClose: false,
      type: ModalConfirmTypes.ERROR,
    });

    if (axios.isCancel(error)) {
      return Promise.reject(error);
    }

    // Network error
    if (!error.response) {
      confirmError.open();
      return Promise.reject(error);
    }

    const { status, data } = error.response;

    if (status >= 500) {
      await router.push({ name: '500' });
    } else if (status === 404) {
      await router.push({ name: '404' });
    } else if (status === 401) {
      await router.push({ name: 'login' });

      const apiError = new ApiError(ApiErrorNames.API_RESPONSE_ERROR, status, data.detail);
      return Promise.reject(apiError);
    } else {
      confirmError.open({ title: `${i18n.global.t('error')}!`, subTitle: data.detail });

      const apiError = new ApiError(ApiErrorNames.API_RESPONSE_ERROR, status, data.detail);
      return Promise.reject(apiError);
    }
  }

  public setLanguage(locale: string): void {
    this.instance.defaults.headers.common['Accept-Language'] = locale;
  }

  public get<T>(url: string, params?: unknown, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.get<never, T>(url, {
      ...config,
      params,
    });
  }

  public getByID<T>(url: string, id: number | string, params?: unknown, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.get<never, T>(`${url}/${id}`, {
      ...config,
      params,
    });
  }

  public post<T>(url: string, data: unknown, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.post<never, T>(url, data, config);
  }

  public update<T>(url: string, id: number | string, data: unknown, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.put<never, T>(`${url}/${id}`, data, config);
  }

  public updatePartial<T>(url: string, id: number | string, data: unknown, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.patch<never, T>(`${url}/${id}`, data, config);
  }

  public delete<T>(url: string, id: number | string, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.delete<never, T>(`${url}/${id}`, config);
  }
}

export default class Api extends HttpClient {
  private static classInstance?: Api;

  private constructor() {
    super(BASE_API_URL);
  }

  public static get getInstance(): Api {
    if (!this.classInstance) {
      this.classInstance = new Api();
    }
    return this.classInstance;
  }
}

export class ApiV2 extends HttpClient {
  private static classInstance?: ApiV2;

  private constructor() {
    super(BASE_API_URL);
  }

  public static get getInstance(): ApiV2 {
    if (!this.classInstance) {
      this.classInstance = new ApiV2();
    }
    return this.classInstance;
  }

  public updatePartial<T>(url: string, data: unknown, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.patch<never, T>(url, data, config);
  }
}

interface Task {
  id: number;
  run: () => Promise<void>;
  resolve: () => void;
  reject: (error: unknown) => void;
}

export class RequestQueue {
  private queue: Task[] = [];
  private isProcessing = false;
  private taskId = 0;
  private hadErrors = false;
  private successCallback?: () => void;
  private errorCallback?: () => void;
  private timer: number | null = null;

  constructor(successCallback?: () => void, errorCallback?: () => void) {
    this.successCallback = successCallback;
    this.errorCallback = errorCallback;
  }

  async addRequest(task: () => Promise<void>): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      const newTask: Task = {
        id: this.taskId++,
        run: task,
        resolve,
        reject,
      };

      this.queue.push(newTask);
      this.processQueue();
    });
  }

  private async processQueue() {
    if (this.isProcessing) return;

    this.isProcessing = true;
    this.hadErrors = false;
    while (this.queue.length > 0) {
      const currentTask = this.queue.shift()!;
      try {
        await currentTask.run();
        currentTask.resolve();
      } catch (error) {
        this.hadErrors = true;
        currentTask.reject(error);
      }
    }
    this.isProcessing = false;

    this.scheduleCallback();
  }

  private scheduleCallback() {
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = null;
    }

    this.timer = setTimeout(() => {
      if (this.queue.length === 0) {
        if (this.hadErrors && this.errorCallback) {
          this.errorCallback();
        }
        if (!this.hadErrors && this.successCallback) {
          this.successCallback();
        }
      }
    }, 2000);
  }
}
