import type { InjectionKey, Ref } from 'vue';

export const hideActionButton = Symbol() as InjectionKey<Ref<boolean>>;
