export const accountBlocksInBlockRoutes = {
  path: 'course/:course_id/block/:block_id/account_blocks',
  name: 'account-blocks-in-block',
  component: () => import('@/views/account-blocks/AccountBlocksInBlock.vue'),
};

export const accountBlocksInCourseRoutes = {
  path: 'course/:course_id/account_blocks',
  name: 'account-blocks-in-course',
  component: () => import('@/views/account-blocks/AccountBlocksInCourse.vue'),
};
