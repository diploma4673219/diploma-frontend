import route from '@/views/account/SignUp.vue';

const routes = {
  path: 'signup',
  name: 'signup',
  component: route,
};

export default routes;
