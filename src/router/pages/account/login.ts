import route from '@/views/account/SignIn.vue';

const routes = {
  path: 'login',
  name: 'login',
  component: route,
};

export default routes;
