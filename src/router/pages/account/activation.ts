import route from '@/views/account/AuthActivation.vue';

const routes = {
  path: 'confirm',
  name: 'confirmation',
  component: route,
};

export default routes;
