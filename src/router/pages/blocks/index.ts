export const blockEditRoutes = {
  path: 'course/:course_id/block/edit/:block_id?',
  name: 'block-edit',
  component: () => import('@/views/blocks/BlockEdit.vue'),
};

export const blockDetailRoutes = {
  path: 'course/:course_id/block/detail/:block_id?',
  name: 'block-detail',
  component: () => import('@/views/blocks/BlockDetail.vue'),
};
