export const courseDetailRoutes = {
  path: 'course/:course_id',
  name: 'course-detail',
  component: () => import('@/views/courses/CourseDetail.vue'),
};

export const courseEditRoutes = {
  path: 'course/:course_id/edit',
  name: 'course-edit',
  component: () => import('@/views/courses/CourseEdit.vue'),
};

export const courseListingRoutes = {
  path: '/',
  name: 'course-listing',
  component: () => import('@/views/courses/CourseListing.vue'),
};

export const courseInviteRoutes = {
  path: '/invite',
  name: 'course-invite',
  component: () => import('@/views/courses/CourseInvitation.vue'),
};
