export const teamDetailRoutes = {
  path: 'course/:course_id/team/detail/:team_id?',
  name: 'team-detail',
  component: () => import('@/views/teams/TeamDetail.vue'),
};

export const teamInviteRoutes = {
  path: 'course/:course_id/team/invite',
  name: 'team-invite',
  component: () => import('@/views/teams/TeamInvitation.vue'),
};
