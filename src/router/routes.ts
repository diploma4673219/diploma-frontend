import { RouteRecordRaw } from 'vue-router';

// main
import error404 from '@/router/pages/main/Error404';
import error500 from '@/router/pages/main/Error500';
import MainLayout from '@/layouts/MainLayout.vue';

// course
import { courseDetailRoutes, courseInviteRoutes, courseListingRoutes, courseEditRoutes } from '@/router/pages/courses';

// block
import { blockEditRoutes, blockDetailRoutes } from '@/router/pages/blocks';

// account blocks
import { accountBlocksInBlockRoutes, accountBlocksInCourseRoutes } from './pages/account-blocks';

// account
import AuthLayout from '@/layouts/AuthLayout.vue';
import LogIn from '@/router/pages/account/login';
import SignUp from '@/router/pages/account/signup';
import Activation from '@/router/pages/account/activation';

// team
import { teamDetailRoutes, teamInviteRoutes } from './pages/teams';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'main',
    component: MainLayout,
    children: [
      courseDetailRoutes,
      courseListingRoutes,
      blockDetailRoutes,
      blockEditRoutes,
      accountBlocksInBlockRoutes,
      accountBlocksInCourseRoutes,
      teamDetailRoutes,
      teamInviteRoutes,
      courseEditRoutes,
    ],
  },
  {
    path: '/auth',
    component: AuthLayout,
    children: [LogIn, SignUp, Activation],
  },
  courseInviteRoutes,
  error404,
  error500,
  {
    path: '/:pathMatch(.*)*',
    redirect: '/404',
  },
];

export default routes;
