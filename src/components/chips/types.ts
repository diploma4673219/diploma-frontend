export interface ChipItem {
  value: string;
  style?: string;
}
