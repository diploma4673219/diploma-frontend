import { RouteLocationNormalized } from 'vue-router';

export interface GroupItem {
  name: string;
  value: string;
  link?: string;
  routerLink?: string | Partial<RouteLocationNormalized>;
}

export interface Step {
  number: number;
  name: string;
}
