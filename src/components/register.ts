import { RendererElement } from 'vue';
import Loader from './AppLoader.vue';
import Icon from './AppIcon.vue';
import Table from './table/AppTable.vue';
import Card from './cards/AppCard.vue';
import Chips from './chips/AppChips.vue';
import Pagination from './AppPagination.vue';
import Filters from './filters/AppFilters.vue';
import FiltersResetButton from './filters/FiltersResetButton.vue';
import AppSideMenu from './AppSideMenu.vue';
import ChipsGroup from './chips/AppChipsGroup.vue';
import OverflownContainer from './AppOverflownContainer.vue';
import SkeletonPreview from './preview/SkeletonPreview.vue';
import AppImage from './AppImage.vue';
import AppDraggable from './AppDraggable.vue';
import AppSearch from './filters/AppSearch.vue';
import AppTextEditor from './text-editor/AppTextEditor.vue';

// Input components
import Input from './input/AppInput.vue';
import Select from './input/AppSelect.vue';
import Checkbox from './input/AppCheckbox.vue';
import Switch from './input/AppSwitch.vue';
import Textarea from './input/AppTextarea.vue';
import FileInput from './input/AppFileInput.vue';
import FileInputWithUrl from './input/AppFileInputWithUrl.vue';
import InputRange from './input/AppInputRange.vue';
import RadioButton from './input/AppRadioButton.vue';

// Button components
import Button from './controls/AppButton.vue';
import ButtonToggle from './controls/AppButtonToggle.vue';
import ButtonGroup from './controls/AppButtonGroup.vue';
import AppSteps from './controls/AppSteps.vue';

// Form components
import Form from './form/AppForm.vue';
import TextField from './form/TextField.vue';
import SelectField from './form/SelectField.vue';
import HugeFileField from './form/HugeFileField.vue';
import CheckboxField from './form/CheckboxField.vue';
import ArrayField from './form/ArrayField.vue';
import SwitchField from './form/SwitchField.vue';
import HugeTextField from './form/HugeTextField.vue';
import ButtonToggleField from './form/ButtonToggleField.vue';
import TextEditorField from './form/TextEditorField.vue';
import Required from './form/AppRequired.vue';
import RangeField from './form/RangeField.vue';

const registerComponents = (app: RendererElement) => {
  app.component('AppButton', Button);
  app.component('AppLoader', Loader);
  app.component('AppTable', Table);
  app.component('AppCard', Card);
  app.component('AppIcon', Icon);
  app.component('AppChips', Chips);
  app.component('AppPagination', Pagination);
  app.component('AppFilters', Filters);
  app.component('FiltersResetButton', FiltersResetButton);
  app.component('AppButtonGroup', ButtonGroup);
  app.component('AppInput', Input);
  app.component('AppChipsGroup', ChipsGroup);
  app.component('AppSteps', AppSteps);
  app.component('AppSideMenu', AppSideMenu);
  app.component('AppSelect', Select);
  app.component('AppCheckbox', Checkbox);
  app.component('AppSwitch', Switch);
  app.component('AppTextarea', Textarea);
  app.component('AppFileInput', FileInput);
  app.component('AppFileInputWithUrl', FileInputWithUrl);
  app.component('AppButtonToggle', ButtonToggle);
  app.component('AppForm', Form);
  app.component('TextField', TextField);
  app.component('SelectField', SelectField);
  app.component('HugeFileField', HugeFileField);
  app.component('CheckboxField', CheckboxField);
  app.component('ArrayField', ArrayField);
  app.component('SwitchField', SwitchField);
  app.component('HugeTextField', HugeTextField);
  app.component('ButtonToggleField', ButtonToggleField);
  app.component('AppOverflownContainer', OverflownContainer);
  app.component('SkeletonPreview', SkeletonPreview);
  app.component('AppImage', AppImage);
  app.component('AppDraggable', AppDraggable);
  app.component('AppTextEditor', AppTextEditor);
  app.component('AppRequired', Required);
  app.component('TextEditorField', TextEditorField);
  app.component('AppInputRange', InputRange);
  app.component('RangeField', RangeField);
  app.component('AppSearch', AppSearch);
  app.component('AppRadioButton', RadioButton);
};

export default registerComponents;
