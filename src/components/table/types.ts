import { IData } from '@/core/types/base';
import { SortDirections, SortParams } from '@/core/utils/sorting';
import type { PaginationRequest } from '@/core/services/Api';

export interface IHeaderConfiguration<T = string> {
  key: T;
  name?: string;
  shortName?: string;
  sortingField?: string;
  sortable?: boolean;
  flex?: string | number;
  minWidth?: string;
  firstSortDirection?: SortDirections;
  optional?: boolean;
  highlighting?: boolean;
  sticky?: 'left' | 'right';
  stickyGap?: string;
}

export enum HeaderAddKeys {
  CHECKBOX = 'checkbox',
  ACTIONS = 'actions',
}

export type IHeaderKey<T> = keyof T | HeaderAddKeys;

export type TableParams = { pagination: PaginationRequest; sort: SortParams };

export interface TableProps {
  header: IHeaderConfiguration[];
  data: IData[];
  tableParams: TableParams;
  totalCount?: number;
  showTotal?: boolean;
  frontSort?: boolean;
  loading?: boolean;
  emptyTableText?: string;
  enableItemsPerPageDropdown?: boolean;
  enableAllItemsPerPage?: boolean;
}
