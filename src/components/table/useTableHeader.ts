import { computed, Ref, ref, watchEffect } from 'vue';
import { IHeaderConfiguration } from '@/components/table/types';
import { useStorage } from '@vueuse/core';

const LOCAL_STORAGE_KEY = 'cols-state';

type Filters = Ref<Record<string, unknown>>;
type FiltersSwapKeys = Record<string, string | string[]>;

interface IParams {
  header: IHeaderConfiguration[];
  filters?: Filters;
  filtersSwapKeys?: FiltersSwapKeys;
  excludedKeys?: Ref<string[]>;
  localStoragePrefix: string;
}

export default function useTableHeader({
  header,
  filters = ref({}),
  filtersSwapKeys = {},
  excludedKeys = ref([]),
  localStoragePrefix,
}: IParams) {
  const notOptionalCols = header.filter((c) => !c.optional).map((col) => col.key);
  const optionalCols = ref(header.filter((c) => c.optional));
  const colsState = useStorage(`${localStoragePrefix}:${LOCAL_STORAGE_KEY}`, optionalCols.value.slice());

  const headerConfig = computed(() => {
    const colsToShowKeys = colsState.value.map((col) => col.key).concat(notOptionalCols);

    return header.reduce((acc, col) => {
      if (excludedKeys.value.includes(col.key)) return acc;
      if (!colsToShowKeys.includes(col.key)) return acc;

      col.highlighting = handleColHighlighting(col, filters, filtersSwapKeys);

      acc.push(col);
      return acc;
    }, [] as IHeaderConfiguration[]);
  });

  watchEffect(() => {
    optionalCols.value = optionalCols.value.filter((el) => !excludedKeys.value.includes(el.key));
    colsState.value = colsState.value.filter((el) => !excludedKeys.value.includes(el.key));
  });

  return { headerConfig, optionalCols, colsState };
}

function handleColHighlighting(col: IHeaderConfiguration, filters: Filters, filtersSwapKeys: FiltersSwapKeys) {
  if (Array.isArray(filtersSwapKeys[col.key])) {
    return (filtersSwapKeys[col.key] as string[]).some((key) => convertValueToBoolean(filters.value[key]));
  } else {
    const key = (filtersSwapKeys[col.key] ?? col.key) as string;
    return convertValueToBoolean(filters.value[key]);
  }
}

function convertValueToBoolean(value: unknown) {
  return Boolean(value) || Number.isInteger(value);
}
