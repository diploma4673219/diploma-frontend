/**
 * Modifies legacy html to match TipTap document structure,
 * without losing content.
 *
 * Images in paragraphs, links with images inside,
 * links not in paragraphs are taken care of.
 *
 * Images are assumed to be configured as blocks in TipTap.
 */
export function processLegacyHtml(html: string) {
  const container = document.createElement('div');
  container.innerHTML = html;

  let el;

  // Move all images out of paragraphs.
  while ((el = container.querySelector('p > img'))) {
    unwrap(el.parentNode as HTMLElement);
  }

  // Move divs iframes out of paragraphs.
  while ((el = container.querySelector('p > div > iframe[src*="vimeo.com"]'))) {
    unwrap(el.parentNode?.parentNode as HTMLElement);
  }

  // Set vimeo iframes proper attribute.
  while (
    (el = container.querySelector(
      ':not([data-type="vimeo-wrapper"]):not([data-type="video"]) > iframe[src*="vimeo.com"]'
    ))
  ) {
    const parentDiv = el.parentNode as HTMLElement;
    parentDiv.dataset.type = 'vimeo-wrapper';
  }

  return container.innerHTML;
}

/**
 * Move all chldren out of an element, and remove the element.
 */
function unwrap(el: HTMLElement) {
  const parent = el.parentNode;

  // Move all children to the parent element.
  while (el.firstChild) parent?.insertBefore(el.firstChild, el);

  // Remove the empty element.
  parent?.removeChild(el);
}
