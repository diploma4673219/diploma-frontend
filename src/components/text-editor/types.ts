export interface TextEditorLinkFields {
  link: string | null;
  event: string | null;
}

export interface TextEditorButtonFields extends TextEditorLinkFields {
  text: string | null;
}

export interface TextEditorImgFields extends TextEditorLinkFields {
  url: string | null;
  width: string | null;
}

export interface TextEditorVideoFields {
  url: string | null;
  width?: string;
  height?: string;
}
