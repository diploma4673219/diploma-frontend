export const EDITOR_TYPE_ATTR = 'data-type';
export const EDITOR_ID_ATTR = 'data-type-id';
export const API_PREFIX = 'articles';
export const MIX_PANEL_METHOD = 'sentDataToAnalytics';
export const MIX_PANEL_SCRIPT = `<script type="application/javascript" ${EDITOR_TYPE_ATTR}="app-script">function sentDataToAnalytics(label){appAnalytics.sendClick(label);}</script>`;

export const getLinkStyles = (link: string) => (link.startsWith('pregnancy://') ? 'internal-link' : 'external-link');
