import Link from '@tiptap/extension-link';
import { EDITOR_TYPE_ATTR, MIX_PANEL_METHOD, getLinkStyles } from '../constants';
import useModal from '@/components/modal/useModal';
import TextEditorLinkModal from '../components/TextEditorLinkModal.vue';
import type { TextEditorLinkFields } from '../types';
import { Selection } from '@tiptap/pm/state';
import { Node } from '@tiptap/pm/model';

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    customLink: {
      openLinkModal: () => ReturnType;
    };
  }
}

const linkModal = useModal({ content: TextEditorLinkModal, attrs: { clickToClose: false, contentClasses: 'p-5' } });

export const CustomLink = Link.extend({
  priority: 100,
  addAttributes() {
    return {
      ...this.parent?.(),
      onclick: {
        default: null,
        parseHTML: (element) => element.getAttribute('onclick'),
      },
      style: {
        default: null,
        parseHTML: (element) => element.getAttribute('style'),
      },
    };
  },
  addCommands() {
    return {
      ...this.parent?.(),
      openLinkModal:
        () =>
        ({ editor }) => {
          const { node } = this.editor.state.selection as Selection & { node: Node };
          if (node && node.type.name === 'imageInLink') {
            editor.commands.openImageModal();
            return true;
          }
          const { href, onclick } = editor.getAttributes('link');
          const eventMatch = onclick && onclick.match(/\(.{1}(.+?).{1}\)/);

          linkModal.open(
            {
              link: href || null,
              event: eventMatch ? eventMatch[1] : null,
              onClose: linkModal.close,
              onSave: ({ link, event }: TextEditorLinkFields) => {
                if (link) {
                  const handler = event ? `${MIX_PANEL_METHOD}('${event}')` : null;
                  const cssClass = getLinkStyles(link);

                  editor
                    .chain()
                    .focus()
                    .extendMarkRange('link')
                    .setLink({ href: link })
                    .updateAttributes('link', { onclick: handler, class: cssClass })
                    .run();
                } else {
                  editor.chain().focus().extendMarkRange('link').unsetLink().run();
                }
                editor.commands.handleMixPanelScript();
                linkModal.close();
              },
            },
            {
              onClosed: () => this.editor.commands.focus(),
            }
          );
          return true;
        },
    };
  },
  parseHTML() {
    return [{ tag: `a[href]:not([href *= "javascript:" i]):not(div[${EDITOR_TYPE_ATTR}="app-button"] a)` }];
  },
});
