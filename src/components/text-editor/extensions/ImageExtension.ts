import Image from '@tiptap/extension-image';

export const CustomImage = Image.extend({
  addAttributes() {
    return {
      ...this.parent?.(),
      style: {
        default: 'display: block; max-width: 100%; margin-left: auto; margin-right: auto;',
        parseHTML: (element: HTMLElement) => element.getAttribute('style'),
      },
      width: {
        default: '100%',
        parseHTML: (element: HTMLElement) => element.getAttribute('width'),
      },
    };
  },
  parseHTML() {
    return [
      {
        tag: this.options.allowBase64 ? 'img[src]' : 'img[src]:not([src^="data:"])',
        getAttrs: (node) => (node as HTMLElement).parentNode?.nodeName !== 'A' && null,
        consuming: false,
      },
    ];
  },
  addNodeView() {
    return ({ HTMLAttributes, editor }) => {
      const img = document.createElement('img');

      img.setAttribute('style', HTMLAttributes.style);
      img.setAttribute('width', HTMLAttributes.width);
      img.setAttribute('src', HTMLAttributes.src);

      img.addEventListener('click', () => {
        editor.commands.openImageModal({
          url: HTMLAttributes.src,
          width: HTMLAttributes.width,
          link: null,
          event: null,
        });
      });

      return {
        dom: img,
      };
    };
  },
});
