import { Node, mergeAttributes } from '@tiptap/core';
import TextEditorVideoModal from '../components/TextEditorVideoModal.vue';
import useModal from '@/components/modal/useModal';
import { EDITOR_TYPE_ATTR } from '../constants';
import { VueNodeViewRenderer } from '@tiptap/vue-3';
import TextEditorBlock from '../components/TextEditorBlock.vue';
import type { TextEditorVideoFields } from '../types';

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    video: {
      setVideo: (options?: TextEditorVideoFields) => ReturnType;
    };
  }
}

const VIMEO_PLAYER_LINK_BASE = 'https://player.vimeo.com/video/';
const VIMEO_LINK_BASE = 'https://vimeo.com/';
const videoModal = useModal({ content: TextEditorVideoModal, attrs: { clickToClose: false, contentClasses: 'p-5' } });

export const getVideoSrcForTextEditor = (url: string, revert?: boolean) => {
  const splitted = url.trim().split('/');
  const videoId = splitted.length && splitted[splitted.length - 1];
  const base = revert ? VIMEO_LINK_BASE : VIMEO_PLAYER_LINK_BASE;

  return `${base}${videoId}`;
};

export default Node.create({
  name: 'video',
  group: 'block',
  draggable: true,
  atom: true,
  content: 'block*',

  addAttributes() {
    return {
      src: {
        parseHTML: (element) => {
          const iframe = element.querySelector('iframe');
          return iframe?.getAttribute('src');
        },
      },
      width: {
        default: '100%',
        parseHTML: (element) => {
          const iframe = element.querySelector('iframe');
          return iframe?.getAttribute('width');
        },
      },
      height: {
        default: null,
        parseHTML: (element) => {
          const iframe = element.querySelector('iframe');
          return iframe?.getAttribute('height');
        },
      },
    };
  },
  parseHTML() {
    return [{ tag: `div[${EDITOR_TYPE_ATTR}="${this.name}"]` }];
  },
  renderHTML({ HTMLAttributes }) {
    return [
      'div',
      {
        [EDITOR_TYPE_ATTR]: this.name,
        style: 'text-align: center;',
      },
      [
        'iframe',
        mergeAttributes(HTMLAttributes, {
          frameborder: 0,
          allow: 'autoplay; fullscreen; picture-in-picture',
          allowfullscreen: true,
        }),
      ],
    ];
  },
  addCommands() {
    return {
      setVideo:
        (options) =>
        ({ commands, tr }) => {
          videoModal.open(
            {
              url: options?.url ?? null,
              width: options?.width,
              showDelete: Boolean(options?.url),
              onClose: videoModal.close,
              onSave: ({ url, width, height: customHeight }: Required<TextEditorVideoFields>) => {
                if (url) {
                  const playerSrc = getVideoSrcForTextEditor(url);
                  const height = getScaledIframeHeight(width, customHeight);

                  this.editor.commands.insertContent(
                    `<div ${EDITOR_TYPE_ATTR}="${this.name}"><iframe src="${playerSrc}"></iframe></div>`
                  );
                  this.editor.commands.updateAttributes('video', { width, height });
                }
                videoModal.close();
              },
              onDelete: () => {
                const { from, to } = tr.selection;
                this.editor.commands.deleteRange({ from, to });
                videoModal.close();
              },
            },
            {
              onClosed: () => this.editor.commands.focus(),
            }
          );
          return commands.setMeta('', '');
        },
    };
  },
  addNodeView() {
    return VueNodeViewRenderer(TextEditorBlock);
  },
});

function getScaledIframeHeight(width: string, height?: string) {
  const defaultIframeHeight = 400;
  return height ?? (parseInt(width) / 100) * defaultIframeHeight;
}
