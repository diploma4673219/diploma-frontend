import { Node, mergeAttributes } from '@tiptap/core';
import useModal from '@/components/modal/useModal';
import TextEditorButtonModal from '../components/TextEditorButtonModal.vue';
import type { TextEditorButtonFields } from '../types';
import { EDITOR_TYPE_ATTR, MIX_PANEL_METHOD } from '../constants';

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    appButton: {
      setButton: (options?: TextEditorButtonFields) => ReturnType;
    };
  }
}

const buttonModal = useModal({ content: TextEditorButtonModal, attrs: { clickToClose: false, contentClasses: 'p-5' } });
const wrapperAttrs: Record<string, string> = {
  style: 'display: flex; justify-content: center',
  [EDITOR_TYPE_ATTR]: 'app-button',
};
const buttonAttrs: Record<string, string> = {
  'data-rel': 'external',
  rel: 'noopener',
  target: '_blank',
  style:
    'color: #ffffff;text-align: center;width: 281px;border-radius: 40px;padding: 10px 20px;background-color: #232323;text-decoration: none;cursor: pointer;',
};

export default Node.create({
  name: 'app-button',
  group: 'block',
  draggable: true,
  atom: true,
  content: 'text*',
  defining: true,

  addAttributes() {
    return {
      href: {
        parseHTML: (element) => element.children[0].getAttribute('href'),
      },
      onclick: {
        parseHTML: (element) => element.children[0].getAttribute('onclick'),
      },
      class: {
        default: 'button',
        parseHTML: (element) => element.children[0].getAttribute('class'),
      },
    };
  },
  parseHTML() {
    return [{ tag: `div[${EDITOR_TYPE_ATTR}="${this.name}"]` }];
  },
  renderHTML({ HTMLAttributes, node }) {
    return [
      'div',
      mergeAttributes(wrapperAttrs, { [EDITOR_TYPE_ATTR]: this.name }),
      ['a', mergeAttributes(buttonAttrs, HTMLAttributes), node.textContent],
    ];
  },
  addCommands() {
    return {
      setButton:
        (options) =>
        ({ tr }) => {
          buttonModal.open(
            {
              text: options?.text ?? null,
              link: options?.link ?? null,
              event: options?.event ?? null,
              showDelete: Boolean(options?.text && options?.link),
              onClose: buttonModal.close,
              onSave: ({ text, link, event }: TextEditorButtonFields) => {
                if (text && link) {
                  this.editor
                    .chain()
                    .focus(null, { scrollIntoView: false })
                    .insertContent({
                      type: this.name,
                      attrs: { href: link.trim(), onclick: event ? `${MIX_PANEL_METHOD}('${event.trim()}')` : null },
                      content: [
                        {
                          type: 'text',
                          text: text.trim(),
                        },
                      ],
                    })
                    .run();
                }
                this.editor.commands.handleMixPanelScript();
                buttonModal.close();
              },
              onDelete: () => {
                const { from, to } = tr.selection;
                this.editor.commands.deleteRange({ from, to });
                this.editor.commands.handleMixPanelScript();
                buttonModal.close();
              },
            },
            {
              onClosed: () => this.editor.commands.focus(),
            }
          );
          return true;
        },
    };
  },
  addNodeView() {
    return ({ node, HTMLAttributes, editor }) => {
      const div = document.createElement('div');
      const a = document.createElement('a');
      const eventMatch = HTMLAttributes.onclick?.match(/\(.{1}(.+?).{1}\)/);
      const event = eventMatch && eventMatch[1];

      Object.keys(wrapperAttrs).forEach((key) => div.setAttribute(key, wrapperAttrs[key]));
      a.setAttribute('style', buttonAttrs.style);
      a.style.setProperty('pointer-events', 'auto', 'important');

      a.addEventListener('click', (e) => {
        e.stopPropagation();
        editor.commands.setButton({ text: node.textContent, link: HTMLAttributes.href, event });
      });

      a.textContent = node.textContent;
      div.append(a);
      return {
        dom: div,
      };
    };
  },
});
