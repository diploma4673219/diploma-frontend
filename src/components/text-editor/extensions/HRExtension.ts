import HorizontalRule from '@tiptap/extension-horizontal-rule';

export const CustomHorizontalRule = HorizontalRule.extend({
  addAttributes() {
    return {
      ...this.parent?.(),
      style: {
        default: 'width: 100%;',
        parseHTML: (element: HTMLElement) => element.getAttribute('style'),
      },
    };
  },
});
