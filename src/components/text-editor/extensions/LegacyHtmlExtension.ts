import { Node } from '@tiptap/core';
import TextEditorHtmlWithPreviewModal from '../components/TextEditorHtmlWithPreviewModal.vue';
import useModal from '@/components/modal/useModal';
import { EDITOR_ID_ATTR, EDITOR_TYPE_ATTR } from '../constants';
import { VueNodeViewRenderer } from '@tiptap/vue-3';
import TextEditorBlock from '../components/TextEditorBlock.vue';
import { Fragment } from '@tiptap/pm/model';

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    legacyHtml: {
      convertLegacyToRaw: (id: string) => ReturnType;
    };
  }
}

const htmlModal = useModal({
  content: TextEditorHtmlWithPreviewModal,
  attrs: { clickToClose: false, contentClasses: 'p-5' },
});

export default Node.create({
  name: 'legacy-html',
  group: 'block',
  draggable: true,
  atom: true,
  content: 'block*',

  addAttributes() {
    return {
      [EDITOR_TYPE_ATTR]: {
        default: null,
      },
      [EDITOR_ID_ATTR]: {
        default: null,
      },
    };
  },
  parseHTML() {
    return [
      {
        tag: `div[${EDITOR_TYPE_ATTR}="${this.name}"]`,
        getContent: () => Fragment.empty,
        getAttrs: (element) => {
          const el = element as HTMLElement;

          const div = document.createElement('div');
          const clone = el.cloneNode(true);
          const attrs = el.getAttributeNames().reduce(
            (map, name) => {
              map[name] = el.getAttribute(name) ?? '';
              return map;
            },
            {} as Record<string, string>
          );
          div.append(clone);

          const wrapper = div.children[0];
          const id = crypto.randomUUID();
          this.storage[id] = wrapper.innerHTML;

          return { ...attrs, [EDITOR_TYPE_ATTR]: this.name, [EDITOR_ID_ATTR]: id };
        },
      },
    ];
  },
  renderHTML({ HTMLAttributes }) {
    return document.createRange().createContextualFragment(this.storage[HTMLAttributes[EDITOR_ID_ATTR]]);
  },
  addCommands() {
    return {
      convertLegacyToRaw:
        (id) =>
        ({ commands, tr }) => {
          htmlModal.open(
            {
              html: this.storage[id] ?? null,
              showDelete: Boolean(this.storage[id]),
              onClose: htmlModal.close,
              onSave: (html: string) => {
                if (html) {
                  this.editor.storage['raw-html'][id] = html;
                  this.editor.commands.insertContent(
                    `<div ${EDITOR_TYPE_ATTR}="raw-html" ${EDITOR_ID_ATTR}="${id}"></div>`
                  );
                }
                htmlModal.close();
              },
              onDelete: () => {
                const { from, to } = tr.selection;
                this.editor.commands.deleteRange({ from, to });
                htmlModal.close();
              },
            },
            {
              onClosed: () => this.editor.commands.focus(),
            }
          );

          return commands.setMeta('', '');
        },
    };
  },
  addNodeView() {
    return VueNodeViewRenderer(TextEditorBlock);
  },
});
