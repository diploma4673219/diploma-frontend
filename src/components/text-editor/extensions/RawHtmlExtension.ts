import { Node } from '@tiptap/core';
import { Fragment } from '@tiptap/pm/model';
import TextEditorHtmlModal from '../components/TextEditorHtmlModal.vue';
import useModal from '@/components/modal/useModal';
import { EDITOR_ID_ATTR, EDITOR_TYPE_ATTR } from '../constants';
import { VueNodeViewRenderer } from '@tiptap/vue-3';
import TextEditorBlock from '../components/TextEditorBlock.vue';

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    rawHtml: {
      setHtmlBlock: (id?: string) => ReturnType;
    };
  }
}

const htmlModal = useModal({ content: TextEditorHtmlModal, attrs: { clickToClose: false, contentClasses: 'p-5' } });

export default Node.create({
  name: 'raw-html',
  group: 'block',
  draggable: true,
  atom: true,
  content: 'block*',

  addAttributes() {
    return {
      [EDITOR_TYPE_ATTR]: this.name,
      [EDITOR_ID_ATTR]: {
        parseHTML: (element) => element.getAttribute(EDITOR_ID_ATTR),
      },
    };
  },
  parseHTML() {
    return [
      {
        tag: `div[${EDITOR_TYPE_ATTR}="${this.name}"]`,
        getContent: (fragment) => {
          const div = document.createElement('div');
          const clone = fragment.cloneNode(true);
          div.append(clone);

          const wrapper = div.children[0];
          const content = wrapper.innerHTML;
          const id = wrapper.getAttribute(EDITOR_ID_ATTR);

          if (content && id && !this.storage[id]) {
            this.storage[id] = content;
          }
          return Fragment.empty;
        },
      },
    ];
  },
  renderHTML({ HTMLAttributes }) {
    const div = document.createElement('div');
    const content = this.storage[HTMLAttributes[EDITOR_ID_ATTR]];

    Object.keys(HTMLAttributes).forEach((key) => div.setAttribute(key, HTMLAttributes[key]));
    div.innerHTML = content;

    return div;
  },
  addCommands() {
    return {
      setHtmlBlock:
        (id) =>
        ({ commands, tr }) => {
          const _id = id ?? crypto.randomUUID();

          htmlModal.open(
            {
              html: this.storage[_id] ?? null,
              showDelete: Boolean(this.storage[_id]),
              onClose: htmlModal.close,
              onSave: (html: string) => {
                if (html) {
                  this.storage[_id] = html;
                  this.editor.commands.insertContent(
                    `<div ${EDITOR_TYPE_ATTR}="${this.name}" ${EDITOR_ID_ATTR}="${_id}"></div>`
                  );
                }
                htmlModal.close();
              },
              onDelete: () => {
                const { from, to } = tr.selection;

                this.editor.commands.deleteRange({ from, to });
                htmlModal.close();
              },
            },
            {
              onClosed: () => this.editor.commands.focus(),
            }
          );

          return commands.setMeta('', '');
        },
    };
  },
  addNodeView() {
    return VueNodeViewRenderer(TextEditorBlock);
  },
});
