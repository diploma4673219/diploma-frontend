import { mergeAttributes, Node } from '@tiptap/core';
import useModal from '@/components/modal/useModal';
import TextEditorImgModal from '../components/TextEditorImgModal.vue';
import { TextEditorImgFields } from '../types';
import { MIX_PANEL_METHOD, EDITOR_ID_ATTR } from '../constants';

export interface ImageOptions {
  HTMLAttributes: Record<string, unknown>;
}

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    imageInLink: {
      openImageModal: (options?: TextEditorImgFields) => ReturnType;
    };
  }
}
const imgModal = useModal({ content: TextEditorImgModal, attrs: { clickToClose: false, contentClasses: 'p-5' } });

export const ImageInLink = Node.create<ImageOptions>({
  name: 'imageInLink',

  addOptions() {
    return {
      HTMLAttributes: {},
    };
  },
  inline: true,
  group: 'inline',
  draggable: true,
  defining: true,

  addAttributes() {
    return {
      src: {
        default: null,
      },
      alt: {
        default: null,
      },
      title: {
        default: null,
      },
      style: {
        default: 'max-width: 100%; margin-left: auto; margin-right: auto;',
        parseHTML: (element: HTMLElement) => element.getAttribute('style'),
      },
      [EDITOR_ID_ATTR]: {
        default: null,
      },
      width: {
        default: '100%',
        parseHTML: (element: HTMLElement) => element.getAttribute('width'),
      },
    };
  },

  parseHTML() {
    return [
      {
        tag: 'a > img[src]',
        getAttrs: (node) => {
          const parent = (node as HTMLElement).parentNode as HTMLElement;
          if (parent?.nodeName === 'A') {
            const link = parent.getAttribute('href');
            const eventMatch = parent.getAttribute('onclick')?.match(/\(.{1}(.+?).{1}\)/);
            const event = eventMatch && eventMatch[1];

            const attrs = (node as HTMLElement).getAttributeNames().reduce(
              (map, name) => {
                map[name] = (node as HTMLElement).getAttribute(name) ?? '';
                return map;
              },
              {} as Record<string, string>
            );
            const id = attrs[EDITOR_ID_ATTR] ?? crypto.randomUUID();
            this.storage[id] = { link, event };

            return { ...attrs, [EDITOR_ID_ATTR]: id };
          } else {
            return false;
          }
        },
        consuming: false,
      },
    ];
  },

  renderHTML({ HTMLAttributes }) {
    return ['img', mergeAttributes(this.options.HTMLAttributes, HTMLAttributes)];
  },

  addCommands() {
    return {
      openImageModal:
        (options) =>
        ({ tr }) => {
          imgModal.open(
            {
              url: options?.url ?? null,
              link: options?.link ?? null,
              event: options?.event ?? null,
              width: options?.width ?? null,
              showDelete: Boolean(options?.url),
              onClose: imgModal.close,
              onSave: ({ url, event, link, width }: TextEditorImgFields) => {
                if (link) {
                  const style = getWidthDependStyles(width);
                  const listener = event ? `onclick="${MIX_PANEL_METHOD}('${event}')"` : '';
                  this.editor.commands.insertContent(
                    `<a href="${link}" style="${style}" ${listener}><img src="${url}" width="${width}" /></a>`
                  );
                } else {
                  this.editor
                    .chain()
                    .focus()
                    .setImage({ src: url as string })
                    .run();
                }
                this.editor.commands.updateAttributes('image', { width });
                this.editor.commands.handleMixPanelScript();
                imgModal.close();
              },
              onDelete: () => {
                const { from, to } = tr.selection;

                this.editor.commands.deleteRange({ from, to });
                this.editor.commands.handleMixPanelScript();
                imgModal.close();
              },
            },
            {
              onClosed: () => this.editor.commands.focus(),
            }
          );
          return true;
        },
    };
  },
  addNodeView() {
    return ({ HTMLAttributes, editor }) => {
      const img = document.createElement('img');
      const { link, event } = this.storage[HTMLAttributes[EDITOR_ID_ATTR]];

      img.setAttribute('style', HTMLAttributes.style);
      img.setAttribute('width', HTMLAttributes.width);
      img.setAttribute('src', HTMLAttributes.src);

      img.addEventListener('click', (e) => {
        e.stopPropagation();
        editor.commands.openImageModal({ url: HTMLAttributes.src, width: HTMLAttributes.width, link, event });
      });

      return {
        dom: img,
      };
    };
  },
});

function getWidthDependStyles(width: string | null) {
  const horizontalMargin = (100 - parseInt(width || '0')) / 2;
  return `margin: 0 ${horizontalMargin}%`;
}
