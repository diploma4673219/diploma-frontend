import { Node, findChildren } from '@tiptap/core';
import { EDITOR_TYPE_ATTR, MIX_PANEL_METHOD, MIX_PANEL_SCRIPT } from '../constants';

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    appScript: {
      handleMixPanelScript: () => ReturnType;
    };
  }
}

export default Node.create({
  name: 'app-script',
  group: 'block',
  atom: true,
  content: 'block*',

  addAttributes() {
    return {
      [EDITOR_TYPE_ATTR]: {
        default: null,
      },
      type: {
        default: null,
      },
    };
  },
  parseHTML() {
    return [
      {
        tag: `script[${EDITOR_TYPE_ATTR}="${this.name}"]`,
        getAttrs: (element) => {
          const el = element as HTMLElement;
          const attrs = el.getAttributeNames().reduce(
            (map, name) => {
              map[name] = el.getAttribute(name) ?? '';
              return map;
            },
            {} as Record<string, string>
          );

          return { ...attrs, [EDITOR_TYPE_ATTR]: this.name };
        },
      },
    ];
  },
  renderHTML({ HTMLAttributes, node }) {
    const script = document.createElement('script');
    const code = document.createTextNode(node.textContent);
    Object.keys(HTMLAttributes).forEach((key) => script.setAttribute(key, HTMLAttributes[key]));

    script.append(code);
    return script;
  },
  addCommands() {
    return {
      handleMixPanelScript:
        () =>
        ({ commands, tr, editor }) => {
          const content = editor.getHTML();
          const hasListener = new RegExp(`onclick=['"]${MIX_PANEL_METHOD}`).test(content);
          const scriptNode = findChildren(tr.doc, (node) => node.type.name === this.name)?.[0];

          if (hasListener && !scriptNode) {
            const position = tr.doc.content.size;

            return commands.insertContentAt(position, MIX_PANEL_SCRIPT);
          } else if (!hasListener && scriptNode) {
            return commands.deleteRange({
              from: scriptNode.pos,
              to: scriptNode.pos + scriptNode.node.nodeSize,
            });
          }
          return true;
        },
    };
  },
  onUpdate() {
    const scriptNode = findChildren(this.editor.state.doc, (node) => node.type.name === this.name)?.[0];

    if (scriptNode && scriptNode.node !== this.editor.state.doc.lastChild) {
      const deleteToPos = scriptNode.pos + scriptNode.node.nodeSize;
      const insertAtPos = this.editor.state.doc.content.size;

      this.editor
        .chain()
        .insertContentAt(insertAtPos, MIX_PANEL_SCRIPT)
        .deleteRange({
          from: scriptNode.pos,
          to: deleteToPos,
        })
        .run();
    }
  },
  addNodeView() {
    return () => {
      const div = document.createElement('div');
      div.style.display = 'none';
      return { dom: div };
    };
  },
});
