import StarterKit from '@tiptap/starter-kit';
import TextAlign from '@tiptap/extension-text-align';
import Highlight from '@tiptap/extension-highlight';
import ButtonExtension from './ButtonExtension';
import RawHtmlExtension from './RawHtmlExtension';
import VideoExtension from './VideoExtension';
import LegacyHtmlExtension from './LegacyHtmlExtension';
import { CustomImage } from './ImageExtension';
import { ImageInLink } from './ImageInLinkExtension';
import { CustomLink } from './LinkExtension';
import ScriptExtension from './ScriptExtension';
import { CustomHorizontalRule } from './HRExtension';

export function getEditorExtensions() {
  return [
    StarterKit.configure({
      horizontalRule: false,
    }),
    TextAlign.configure({
      types: ['heading', 'paragraph'],
    }),
    CustomLink.configure({
      autolink: false,
      protocols: ['mailto'],
      openOnClick: false,
    }),
    Highlight.configure({ multicolor: true }),
    CustomImage,
    ImageInLink,
    ButtonExtension,
    RawHtmlExtension,
    VideoExtension,
    LegacyHtmlExtension,
    ScriptExtension,
    CustomHorizontalRule,
  ];
}
