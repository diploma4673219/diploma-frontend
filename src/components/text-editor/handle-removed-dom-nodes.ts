import { EDITOR_TYPE_ATTR } from '@/components/text-editor/constants';

type MutatedNodeData = { node: HTMLElement; order: number };

export function handleRemovedDomNodes(mutation: MutationRecord) {
  const { removed, textNodes } = Array.from(mutation.removedNodes).reduce(
    (acc, n, i) => {
      if (n.nodeType === 1) {
        acc.removed.push({ node: n as HTMLElement, order: i });
      } else if (n.nodeType === 3) {
        const wrapper = document.createElement('p');

        wrapper.append(n);
        wrapper.textContent?.trim() && acc.textNodes.push({ node: wrapper, order: i });
      }
      return acc;
    },
    { removed: [], textNodes: [] } as Record<string, MutatedNodeData[]>
  );

  const added = Array.from(mutation.addedNodes).map((n, i) => ({ node: n, order: i }));

  const merged = (removed as MutatedNodeData[])
    .reduce((acc, element) => {
      const index = added.findIndex((n) => element.node.nodeName === n.node.nodeName);

      if (index === -1) {
        const wrapper = document.createElement('div');
        wrapper.setAttribute(EDITOR_TYPE_ATTR, 'legacy-html');
        wrapper.append(element.node);

        element.node = wrapper;
      } else {
        added.splice(index, 1);
      }

      acc.push(element);
      return acc;
    }, [] as MutatedNodeData[])
    .concat(textNodes)
    .sort((a, b) => a.order - b.order);

  return merged.reduce((acc, n) => `${acc}${n.node.outerHTML}`, '');
}
