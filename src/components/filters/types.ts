export interface IFilterConfiguration {
  key: string;
  name?: string;
  optional?: boolean;
  notInLineWithResetButton?: boolean;
}
