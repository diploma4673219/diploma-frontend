import { computed, ref } from 'vue';
import { IFilterConfiguration } from './types';
import { useStorage } from '@vueuse/core';
import { IData } from '@/core/types/base';

const LOCAL_STORAGE_KEY = 'filters-state';

interface IParams {
  config: IFilterConfiguration[];
  localStoragePrefix: string;
}

export default function useFilters<T extends IData = IData>({ config, localStoragePrefix }: IParams) {
  const notOptionalFilters = config.filter((el) => !el.optional);
  const optionalFilters = ref(config.filter((el) => el.optional));
  const filtersState = useStorage<IFilterConfiguration[]>(`${localStoragePrefix}:${LOCAL_STORAGE_KEY}`, []);

  const filtersRef = ref<T>({} as T);
  const filteredValues = computed(() =>
    Object.entries(filtersRef.value)
      .filter(
        ([key]) =>
          notOptionalFilters.map((el) => el.key).includes(key) || filtersState.value.map((el) => el.key).includes(key)
      )
      .reduce((obj, [key, value]) => ({ ...obj, [key]: value }), {} as T)
  );

  const showFields = computed(() =>
    Object.keys(filtersRef.value).reduce(
      (obj, key) => ({ ...obj, [key]: filtersState.value.map((el) => el.key).includes(key) }),
      {} as { [P in keyof Required<T>]: boolean }
    )
  );
  const showResetButton = computed(
    () => filtersState.value.length + notOptionalFilters.filter((el) => !el.notInLineWithResetButton).length > 1
  );
  const disableResetButton = computed(() =>
    Object.values(filteredValues.value).every((el) => el === undefined || el === null)
  );

  return { filtersRef, filteredValues, filtersState, optionalFilters, showFields, showResetButton, disableResetButton };
}
