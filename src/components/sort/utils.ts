import { SortDirections } from '@/core/utils/sorting';

export const DEFAULT_SORT_DIRECTION = SortDirections.ASC;

export const apiPrefixMap = {
  [SortDirections.ASC]: '',
  [SortDirections.DESC]: '-',
};

export const swapSortDirection = {
  [SortDirections.ASC]: SortDirections.DESC,
  [SortDirections.DESC]: SortDirections.ASC,
};

export const enum SortStates {
  DEFAULT = 0,
  FIRST_STATE = 1,
  REVERSED_STATE = 2,
}

export const swapSortState = {
  [SortStates.DEFAULT]: SortStates.FIRST_STATE,
  [SortStates.FIRST_STATE]: SortStates.REVERSED_STATE,
  [SortStates.REVERSED_STATE]: SortStates.DEFAULT,
};
