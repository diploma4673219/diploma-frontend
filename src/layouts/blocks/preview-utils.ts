export function getPreviewHead() {
  const addStyles = `
    body { width: 100%; overflow-x: hidden; padding-bottom: 1em; }
    html > body { font-size: 16px; line-height: 24px; }
    body::-webkit-scrollbar { display: none }
    p {
      overflow-wrap: break-word;
      word-wrap: break-word;
      -ms-word-break: break-all;
      word-break: break-word;
      -ms-hyphens: auto;
      -moz-hyphens: auto;
      -webkit-hyphens: auto;
      hyphens: auto;
    }
  `;

  const styles = `<style>${addStyles}</style>`;

  return styles;
}
