import { number, AnySchema, object, string } from 'yup';

export const schemaCreateScore: AnySchema = object().shape({
  score: number().nullable().required(),
});

export const schemaCreateAnswer: AnySchema = object().shape({
  file: string().nullable().staticLink().required(),
  block_id: number().required(),
});
