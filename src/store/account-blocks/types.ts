import { ErrorBase, IData } from '@/core/types/base';
import { IUser } from '../account/types';
import { IBlock, IBlockApi } from '../blocks/types';
import { PaginationRequest, Pagination } from '@/core/services/Api';

export const LOCAL_STORAGE_PREFIX_ACCOUNT_BLOCKS_BLOCK = 'account-blocks-block';

interface IAccountBlockBase extends IData {
  id: number;
  user: IUser;
  block: IBlock | IBlockApi;
  team: string | null;
  file: string;
  score: number | null;
  comment: string | null;
  assistant: IUser | null;
}

export interface IAccountBlock extends IAccountBlockBase {
  block: IBlock;
}

export interface IAccountBlockApiPayload extends IAccountBlockBase {
  block: IBlockApi;
}

export interface IAccountBlockApi {
  result: IAccountBlockApiPayload[];
  pagination: Pagination;
}

export interface IAccountBlockCreate {
  block_id: number;
  file: string;
}

export interface IAccountBlockUpdateAssistent {
  score?: number;
  comment?: string | null;
  assistant_id?: number | null;
}

export interface IAccountBlockList extends IAccountBlockBase {
  first_name: string;
  last_name: string;
  group: string;
  email: string;
  team: string;
  block: IBlock;
}

export interface IAccountBlockFilters {
  block_id?: number;
  is_author?: boolean;
  is_team?: boolean;
  assistant_id?: number | boolean;
  score?: number;
  search?: string | null;
}

export interface IAccountBlockFiltersApi extends IAccountBlockFilters, PaginationRequest {
  sort?: string;
  assistant_id?: number;
  search?: string;
}

export enum AccountBlockErrors {
  COURSE_ERROR = 'Course error',
}

const blockErrors: Record<AccountBlockErrors, string> = {
  [AccountBlockErrors.COURSE_ERROR]: 'Такой курс не существует',
};

export class AccountBlockError extends ErrorBase<AccountBlockErrors> {
  readonly data: string;

  constructor(name: AccountBlockErrors, data?: IAccountBlock) {
    const message = blockErrors[name];
    super(name, message);

    this.data = data ? JSON.stringify(data) : '';
  }
}

export interface IAccountBlockStatistics {
  total: number;
  total_checked: number;
  scores: Record<string, number>;
}
