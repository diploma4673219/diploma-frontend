import { defineStore, storeToRefs } from 'pinia';
import { computed, ref } from 'vue';
import Api, { ApiError } from '@/core/services/Api';
import {
  IAccountBlock,
  IAccountBlockUpdateAssistent,
  IAccountBlockCreate,
  AccountBlockError,
  AccountBlockErrors,
  IAccountBlockList,
  IAccountBlockFiltersApi,
  IAccountBlockApi,
  IAccountBlockApiPayload,
  IAccountBlockStatistics,
} from './types';
import { ErrorBase, UNKNOWN_ERROR } from '@/core/types/base';
import useCourseStore from '@/store/courses';
import useBlockStore from '@/store/blocks';

const BASE_URL = (courseID: number) => `/courses/${courseID}/user_blocks`;

export default defineStore('user_block', () => {
  const courseStore = useCourseStore();
  const { getCourse } = storeToRefs(courseStore);

  const blockStore = useBlockStore();
  const { handleBlock } = blockStore;

  // State
  const accountBlock = ref<IAccountBlock | null>(null);
  const accountBlocks = ref<IAccountBlock[]>([]);
  const accountBlocksTotal = ref<number>(0);

  // Getters
  const getAccountBlock = computed(() => accountBlock.value);
  const getAccountBlockList = computed(() => accountBlocks.value);
  const getAccountBlockListTotal = computed(() => accountBlocksTotal.value);

  // Setters
  const setAccountBlock = (item: IAccountBlock | null) => (accountBlock.value = item);
  const setAccountBlocks = (items: IAccountBlock[]) => (accountBlocks.value = items);
  const setAccountBlocksTotal = (total: number) => (accountBlocksTotal.value = total);

  // Actions
  function getError(error: unknown) {
    const isApiError = error instanceof ApiError;

    return isApiError ? error : new ErrorBase(UNKNOWN_ERROR, UNKNOWN_ERROR);
  }

  function toList(data: IAccountBlock): IAccountBlockList {
    return {
      ...data,
      first_name: data.user.first_name,
      last_name: data.user.last_name,
      group: data.user.group,
      email: data.user.email,
      comment: data.comment ?? '',
      team: data.team ?? '',
    };
  }

  function handleApi(data: IAccountBlockApiPayload): IAccountBlock {
    return {
      ...data,
      block: handleBlock(data.block),
    };
  }

  const fetchAccountBlock = async (block_id: number) => {
    try {
      if (!getCourse.value) {
        throw new AccountBlockError(AccountBlockErrors.COURSE_ERROR);
      }

      const response = await Api.getInstance.getByID<IAccountBlock | null>(BASE_URL(getCourse.value.id), block_id);
      setAccountBlock(response);
    } catch (error) {
      throw getError(error);
    }
  };

  const fetchAccountBlockList = async (params: IAccountBlockFiltersApi) => {
    try {
      if (!getCourse.value) {
        throw new AccountBlockError(AccountBlockErrors.COURSE_ERROR);
      }

      const { result, pagination } = await Api.getInstance.get<IAccountBlockApi>(BASE_URL(getCourse.value.id), params);
      setAccountBlocks(result.map(handleApi));
      setAccountBlocksTotal(pagination.total_count);
    } catch (error) {
      throw getError(error);
    }
  };

  const createAccountBlock = async (body: IAccountBlockCreate) => {
    try {
      if (!getCourse.value) {
        throw new AccountBlockError(AccountBlockErrors.COURSE_ERROR);
      }

      const response = await Api.getInstance.post<IAccountBlock>(BASE_URL(getCourse.value.id), body);
      setAccountBlock(response);
    } catch (error) {
      throw getError(error);
    }
  };

  const updateAccountBlockUser = async (id: number, body: IAccountBlockCreate) => {
    try {
      if (!getCourse.value) {
        throw new AccountBlockError(AccountBlockErrors.COURSE_ERROR);
      }

      const response = await Api.getInstance.updatePartial<IAccountBlock>(
        `${BASE_URL(getCourse.value.id)}/student`,
        id,
        body
      );

      setAccountBlock(response);
    } catch (error) {
      throw getError(error);
    }
  };

  const updateAccountBlockAssistant = async (id: number, body: IAccountBlockUpdateAssistent) => {
    try {
      if (!getCourse.value) {
        throw new AccountBlockError(AccountBlockErrors.COURSE_ERROR);
      }

      const response = await Api.getInstance.updatePartial<IAccountBlock>(
        `${BASE_URL(getCourse.value.id)}/assistant`,
        id,
        body
      );

      const blockIndex = accountBlocks.value.findIndex((item) => item.id === response.id);

      if (blockIndex != -1) {
        accountBlocks.value[blockIndex] = {
          ...response,
        };
      }
    } catch (error) {
      throw getError(error);
    }
  };

  const getScorecard = async (params: IAccountBlockFiltersApi) => {
    try {
      if (!getCourse.value) {
        throw new AccountBlockError(AccountBlockErrors.COURSE_ERROR);
      }

      await Api.getInstance.get(`${BASE_URL(getCourse.value.id)}/all/csv`, params);
    } catch (error) {
      throw getError(error);
    }
  };

  const getStatistics = async (params: IAccountBlockFiltersApi) => {
    try {
      if (!getCourse.value) {
        throw new AccountBlockError(AccountBlockErrors.COURSE_ERROR);
      }

      const result = await Api.getInstance.get<IAccountBlockStatistics>(
        `${BASE_URL(getCourse.value.id)}/all/statistics`,
        params
      );

      return result;
    } catch (error) {
      throw getError(error);
    }
  };

  return {
    getAccountBlock,
    getAccountBlockList,
    createAccountBlock,
    updateAccountBlockAssistant,
    updateAccountBlockUser,
    fetchAccountBlock,
    fetchAccountBlockList,
    toList,
    setAccountBlock,
    setAccountBlocks,
    getAccountBlockListTotal,
    getScorecard,
    getStatistics,
  };
});
