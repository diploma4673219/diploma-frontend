import { ErrorBase } from '@/core/types/base';
import { IUser } from '../account/types';

export const TEAM_TOKEN_KEY = 'team_token';
export const TEAM_COURSE_KEY = 'team_course';

export interface IUserTeam {
  id: number;
  user: IUser;
  team_id: number;
  is_leader: boolean;
}

export interface ITeam {
  id: number;
  course_id: number;
  name: string;
  user_teams: IUserTeam[];
}

export interface ITeamEdit {
  name: string;
}

export enum TeamErrors {
  COURSE_ERROR = 'Course error',
}

const blockErrors: Record<TeamErrors, string> = {
  [TeamErrors.COURSE_ERROR]: 'Такой курс не существует',
};

export class TeamError extends ErrorBase<TeamErrors> {
  readonly data: string;

  constructor(name: TeamErrors, data?: ITeam) {
    const message = blockErrors[name];
    super(name, message);

    this.data = data ? JSON.stringify(data) : '';
  }
}
