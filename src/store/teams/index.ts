import { defineStore, storeToRefs } from 'pinia';
import { computed, ref } from 'vue';
import { ApiError, ApiV2 as Api } from '@/core/services/Api';
import { ITeam, ITeamEdit, TeamError, TeamErrors } from './types';
import { ErrorBase, UNKNOWN_ERROR } from '@/core/types/base';
import useCourseStore from '@/store/courses';
import useAccountStore from '@/store/account';

const BASE_URL = (courseID: number) => `/courses/${courseID}/teams`;

export default defineStore('team', () => {
  const courseStore = useCourseStore();
  const { getCourse } = storeToRefs(courseStore);

  const accountStore = useAccountStore();
  const { getAccount } = storeToRefs(accountStore);

  // State
  const team = ref<ITeam | null>(null);
  const teams = ref<ITeam[]>([]);

  // Getters
  const getTeam = computed(() => team.value);
  const getTeamList = computed(() => teams.value);
  const getMember = computed(() => team.value?.user_teams.find((el) => el.user.id === getAccount.value?.id) ?? null);
  const getIsLeader = computed(() => getMember.value?.is_leader ?? false);

  // Setters
  const setTeam = (item: ITeam | null) => (team.value = item);
  const setTeamList = (items: ITeam[]) => (teams.value = items);

  // Actions
  function getError(error: unknown) {
    const isApiError = error instanceof ApiError;

    return isApiError ? error : new ErrorBase(UNKNOWN_ERROR, UNKNOWN_ERROR);
  }

  const fetchTeam = async (id: number = -1) => {
    try {
      if (!getCourse.value) {
        throw new TeamError(TeamErrors.COURSE_ERROR);
      }

      const response = await Api.getInstance.getByID<ITeam | null>(BASE_URL(getCourse.value.id), id);
      setTeam(response);
    } catch (error) {
      throw getError(error);
    }
  };

  const fetchTeamList = async () => {
    try {
      if (!getCourse.value) {
        throw new TeamError(TeamErrors.COURSE_ERROR);
      }

      const result = await Api.getInstance.get<ITeam[]>(BASE_URL(getCourse.value.id));
      setTeamList(result);
    } catch (error) {
      throw getError(error);
    }
  };

  const createTeam = async (body: ITeamEdit) => {
    try {
      if (!getCourse.value) {
        throw new TeamError(TeamErrors.COURSE_ERROR);
      }

      const response = await Api.getInstance.post<ITeam>(BASE_URL(getCourse.value.id), body);
      setTeam(response);
    } catch (error) {
      throw getError(error);
    }
  };

  const updateTeam = async (body: ITeamEdit) => {
    try {
      if (!getCourse.value) {
        throw new TeamError(TeamErrors.COURSE_ERROR);
      }

      const response = await Api.getInstance.updatePartial<ITeam>(`${BASE_URL(getCourse.value.id)}`, body);
      setTeam(response);
    } catch (error) {
      throw getError(error);
    }
  };

  const deleteTeam = async (id: number = -1) => {
    try {
      if (!getCourse.value) {
        throw new TeamError(TeamErrors.COURSE_ERROR);
      }

      await Api.getInstance.delete<ITeam>(BASE_URL(getCourse.value.id), id);
    } catch (error) {
      throw getError(error);
    }
  };

  const getMemberLink = async () => {
    try {
      if (!getCourse.value) {
        throw new TeamError(TeamErrors.COURSE_ERROR);
      }

      const result = await Api.getInstance.get<{ team_token: string }>(`${BASE_URL(getCourse.value.id)}/member/invite`);
      return result.team_token;
    } catch (error) {
      throw getError(error);
    }
  };

  const inviteUser = async (team_token: string) => {
    try {
      if (!getCourse.value) {
        throw new TeamError(TeamErrors.COURSE_ERROR);
      }

      await Api.getInstance.post(`${BASE_URL(getCourse.value.id)}/member`, {
        team_token,
      });
    } catch (error) {
      throw getError(error);
    }
  };

  const deleteUser = async (member_id: number = -1) => {
    try {
      if (!getCourse.value) {
        throw new TeamError(TeamErrors.COURSE_ERROR);
      }

      await Api.getInstance.delete(`${BASE_URL(getCourse.value.id)}/member`, member_id);

      const index = team.value?.user_teams.findIndex((el) => el.id === getAccount.value?.id) ?? -1;
      team.value?.user_teams.splice(index, 1);
    } catch (error) {
      throw getError(error);
    }
  };

  return {
    getTeam,
    getTeamList,
    fetchTeam,
    fetchTeamList,
    createTeam,
    updateTeam,
    deleteTeam,
    getMemberLink,
    getIsLeader,
    inviteUser,
    deleteUser,
  };
});
