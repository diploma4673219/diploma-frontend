import { defineStore } from 'pinia';
import { ref, computed } from 'vue';
import Api, { AUTH_TOKEN_KEY, ApiError } from '@/core/services/Api';
import { IUser, IUserCreate, IAccessToken } from './types';
import { useStorage } from '@vueuse/core';
import { ErrorBase, UNKNOWN_ERROR } from '@/core/types/base';

const BASE_URL = '/users';

export default defineStore('account', () => {
  // State
  const account = ref<IUser | null>(null);
  const accessToken = useStorage<string | null>(AUTH_TOKEN_KEY, null);

  // Getters
  const getAccount = computed<IUser | null>(() => account.value);
  const isUserAuthenticated = computed<boolean>(() => !!accessToken.value);

  // Actions
  function getError(error: unknown) {
    const isApiError = error instanceof ApiError;

    return isApiError ? error : new ErrorBase(UNKNOWN_ERROR, UNKNOWN_ERROR);
  }

  const logOut = () => {
    accessToken.value = null;
  };

  const logIn = async (credentials: IUserCreate) => {
    try {
      const { access_token } = await Api.getInstance.post<IAccessToken>(`${BASE_URL}/token`, credentials);
      accessToken.value = access_token;
    } catch (error) {
      throw getError(error);
    }
  };

  const signUp = async (credentials: IUserCreate) => {
    try {
      delete credentials['re_password'];
      delete credentials['agree'];

      const { access_token } = await Api.getInstance.post<IAccessToken>(BASE_URL, credentials);
      accessToken.value = access_token;
    } catch (error) {
      throw getError(error);
    }
  };

  const fetchUser = async () => {
    try {
      const response = await Api.getInstance.get<IUser>(`${BASE_URL}/me`);
      account.value = response;
    } catch (error) {
      throw getError(error);
    }
  };

  const updateUser = async (data: IUser) => {
    try {
      const response = await Api.getInstance.updatePartial<IUser>(BASE_URL, 'me', data);
      account.value = response;
    } catch (error) {
      throw getError(error);
    }
  };

  const confirm = async (email_token: string) => {
    try {
      const data = {
        email_token,
      };

      const { access_token } = await Api.getInstance.post<IAccessToken>(`${BASE_URL}/verify_email`, data);
      accessToken.value = access_token;
    } catch (error) {
      throw getError(error);
    }
  };

  return {
    getAccount,
    isUserAuthenticated,
    logOut,
    logIn,
    signUp,
    account,
    fetchUser,
    updateUser,
    confirm,
  };
});
