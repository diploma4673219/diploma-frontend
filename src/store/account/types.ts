import { IData } from '@/core/types/base';

export interface IUser extends IData {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  middle_name: string | null;
  group: string;
  is_active: boolean;
  avatar: string | null;
}

export interface IUserCreate {
  email: string;
  password: string;
  re_password?: string;
  agree?: boolean;
}

export interface IAccessToken {
  access_token: string;
}
