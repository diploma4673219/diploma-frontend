import { ErrorBase, IData, IDataSelect } from '@/core/types/base';

export interface ITeamSettings {
  members_amount_min: number;
  members_amount_max: number | null;
}

export interface ICourseToken {
  course_token: string;
}

export const COURSE_TOKEN_KEY = 'course_token';

export enum UsersRoles {
  MAINTAINER = 0,
  ASSISTANT = 1,
  STUDENT = 2,
  ASSISTANT_REQUEST = 3,
  STUDENT_REQUEST = 4,
  ASSISTANT_APPROVE = 5,
}

export interface IUserRole extends IDataSelect {
  value: UsersRoles;
}

export const usersRoles: IUserRole[] = [
  { id: 1, name: 'Преподаватель', value: UsersRoles.MAINTAINER },
  { id: 2, name: 'Ассистент', value: UsersRoles.ASSISTANT },
  { id: 3, name: 'Студент', value: UsersRoles.STUDENT },
  { id: 4, name: 'Запрос на ассистента', value: UsersRoles.ASSISTANT_REQUEST },
  { id: 5, name: 'Запрос на студента', value: UsersRoles.STUDENT_REQUEST },
  { id: 6, name: 'Неподтвержденный ассистент', value: UsersRoles.ASSISTANT_APPROVE },
];

interface ICourseBase extends IData {
  id: number;
  title: string;
  description: string;
  image: string;
  team_settings: ITeamSettings | null;
  role: IUserRole | number;
}

export interface ICourse extends ICourseBase {
  role: IUserRole;
}

export interface ICourseApi extends ICourseBase {
  role: number;
}

export enum CourseErrors {
  ROLE_ERROR = 'Role error',
}

const courseErrors: Record<CourseErrors, string> = {
  [CourseErrors.ROLE_ERROR]: 'Такая роль не существует',
};

export class CourseError extends ErrorBase<CourseErrors> {
  readonly data: string;

  constructor(name: CourseErrors, data: ICourseBase) {
    const message = courseErrors[name];
    super(name, message);
    this.data = JSON.stringify(data);
  }
}
