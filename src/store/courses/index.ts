import { defineStore } from 'pinia';
import { computed, ref } from 'vue';
import Api, { ApiError } from '@/core/services/Api';
import { ICourse, ICourseApi, usersRoles, CourseError, CourseErrors, ICourseToken, UsersRoles } from './types';
import { ErrorBase, UNKNOWN_ERROR } from '@/core/types/base';

const BASE_URL = '/courses';

export default defineStore('course', () => {
  // State
  const course = ref<ICourse>({} as ICourse);
  const courses = ref<ICourse[]>([]);

  // Getters
  const getCourse = computed(() => course.value);
  const getCourseList = computed(() => courses.value);

  // Actions
  function getError(error: unknown) {
    const isApiError = error instanceof ApiError;
    const isCourseError = error instanceof CourseError;

    return isApiError || isCourseError ? error : new ErrorBase(UNKNOWN_ERROR, UNKNOWN_ERROR);
  }

  function handleCourse(course: ICourseApi): ICourse {
    const role = usersRoles.find((el) => el.value === course.role);
    if (!role) {
      throw new CourseError(CourseErrors.ROLE_ERROR, course);
    }

    return {
      ...course,
      role,
    };
  }

  const fetchCourseList = async () => {
    try {
      const response = await Api.getInstance.get<ICourse[]>(BASE_URL);
      courses.value = response.slice();
    } catch (error) {
      throw getError(error);
    }
  };

  const fetchCourse = async (id: number) => {
    try {
      const response = await Api.getInstance.getByID<ICourseApi>(BASE_URL, id);
      course.value = handleCourse(response);
    } catch (error) {
      throw getError(error);
    }
  };

  const updateCourse = async (id: number = getCourse.value.id, data: Partial<ICourse>) => {
    try {
      const response = await Api.getInstance.updatePartial<ICourseApi>(BASE_URL, id, data);
      course.value = handleCourse(response);
    } catch (error) {
      throw getError(error);
    }
  };

  const getStudentLink = async (id: number = getCourse.value.id) => {
    try {
      const response = await Api.getInstance.get<ICourseToken>(`${BASE_URL}/${id}/invite_student`);
      return response.course_token;
    } catch (error) {
      throw getError(error);
    }
  };

  const getAssistantLink = async (id: number = getCourse.value.id) => {
    try {
      const response = await Api.getInstance.get<ICourseToken>(`${BASE_URL}/${id}/invite_assistant`);
      return response.course_token;
    } catch (error) {
      throw getError(error);
    }
  };

  const inviteUser = async (course_token: string) => {
    try {
      const response = await Api.getInstance.post<{ course: number }>(`${BASE_URL}/role`, {
        course_token,
      });

      return response.course;
    } catch (error) {
      throw getError(error);
    }
  };

  const checkRole = async (id: number = getCourse.value.id) => {
    try {
      const { role } = await Api.getInstance.post<{ role: UsersRoles }>(`${BASE_URL}/${id}/role`, {});

      course.value = handleCourse({
        ...course.value,
        role,
      });
    } catch (error) {
      throw getError(error);
    }
  };

  return {
    getCourse,
    getCourseList,
    fetchCourse,
    fetchCourseList,
    getStudentLink,
    getAssistantLink,
    inviteUser,
    checkRole,
    updateCourse,
  };
});
