import { defineStore } from 'pinia';
import { computed, ref } from 'vue';
import Api, { ApiError } from '@/core/services/Api';
import { IBlock, blockStatuses, BlockErrors, BlockError, IBlockApi, defaultBlock } from './types';
import { ErrorBase, UNKNOWN_ERROR } from '@/core/types/base';
import useCourseStore from '@/store/courses';
import { storeToRefs } from 'pinia';

const BASE_URL = (courseID: number) => `/courses/${courseID}/blocks`;

export default defineStore('block', () => {
  const courseStore = useCourseStore();
  const { getCourse } = storeToRefs(courseStore);

  // State
  const block = ref<IBlock>({} as IBlock);
  const blocks = ref<IBlock[]>([]);

  // Getters
  const getBlock = computed(() => block.value);
  const getBlockList = computed(() => blocks.value);

  // Setters.
  const setBlock = (item: IBlock) => (block.value = item);
  const setBlockList = (items: IBlock[]) => (blocks.value = items);
  const mutateBlockList = (item: IBlock, index: number) => blocks.value.splice(index, 1, item);

  // Actions
  function getError(error: unknown) {
    const isApiError = error instanceof ApiError;
    const isBlockError = error instanceof BlockError;

    return isApiError || isBlockError ? error : new ErrorBase(UNKNOWN_ERROR, UNKNOWN_ERROR);
  }

  function handleDefault(block = {} as IBlock): IBlock {
    return {
      ...defaultBlock,
      ...block,
    };
  }

  function handleBlock(block: IBlockApi): IBlock {
    const status = blockStatuses.find((el) => el.value === block.status);
    if (!status) {
      throw new BlockError(BlockErrors.STATUS_ERROR, block);
    }

    return handleDefault({
      ...block,
      status,
    });
  }

  function handleUpdate(response: IBlockApi) {
    const result = handleBlock(response);

    if (block.value.id === result.id) {
      setBlock(result);
    }

    const blockIndex = blocks.value.findIndex((el) => el.id === result.id);

    if (blockIndex !== -1) {
      mutateBlockList(result, blockIndex);
    }
  }

  function toApiBlock(block: Partial<IBlock>): Partial<IBlockApi> {
    return {
      ...block,
      status: block.status?.value,
    };
  }

  const fetchBlock = async (id: number) => {
    try {
      if (!getCourse.value) {
        throw new BlockError(BlockErrors.COURSE_ERROR, { id } as IBlock);
      }

      const response = await Api.getInstance.getByID<IBlockApi>(BASE_URL(getCourse.value.id), id);
      setBlock(handleBlock(response));
    } catch (error) {
      throw getError(error);
    }
  };

  const fetchBlocks = async () => {
    try {
      if (!getCourse.value) {
        throw new BlockError(BlockErrors.COURSE_ERROR, {} as IBlock);
      }

      const response = await Api.getInstance.get<IBlockApi[]>(BASE_URL(getCourse.value.id));
      setBlockList(response.map(handleBlock));
    } catch (error) {
      throw getError(error);
    }
  };

  const createBlock = async (body: IBlock = block.value) => {
    try {
      if (!getCourse.value) {
        throw new BlockError(BlockErrors.COURSE_ERROR, body);
      }

      body.course_id = getCourse.value.id;

      const response = await Api.getInstance.post<IBlockApi>(BASE_URL(getCourse.value.id), toApiBlock(body));

      getBlockList.value.push(handleBlock(response));
    } catch (error) {
      throw getError(error);
    }
  };

  const updateBlock = async (body: Partial<IBlock> = block.value, id = block.value.id) => {
    try {
      if (!getCourse.value) {
        throw new BlockError(BlockErrors.COURSE_ERROR, { id } as IBlock);
      }

      const response = await Api.getInstance.updatePartial<IBlockApi>(
        BASE_URL(getCourse.value.id),
        id,
        toApiBlock(body)
      );
      handleUpdate(response);
    } catch (error) {
      throw getError(error);
    }
  };

  const reOrderBlockList = async () => {
    if (!getCourse.value) {
      throw new BlockError(BlockErrors.COURSE_ERROR, {} as IBlock);
    }

    const body = blocks.value.map(({ id, order }) => {
      return {
        id,
        order,
      };
    });

    try {
      await Api.getInstance.post(`${BASE_URL(getCourse.value.id)}/bulk_reorder`, body);
    } catch (error) {
      throw getError(error);
    }
  };

  return {
    getBlock,
    getBlockList,
    fetchBlock,
    fetchBlocks,
    createBlock,
    updateBlock,
    setBlock,
    handleDefault,
    block,
    blocks,
    reOrderBlockList,
    handleBlock,
  };
});
