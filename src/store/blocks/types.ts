import { ErrorBase, IData, IDataSelect } from '@/core/types/base';

export enum BlockStatuses {
  HIDE = 0,
  OPEN = 1,
  ON_CHECK = 2,
  CHECKED = 3,
  ARCHIVE = 4,
}

export interface IBlockStatus extends IDataSelect {
  value: BlockStatuses;
  icon: string;
}

export const blockStatuses: IBlockStatus[] = [
  { id: 1, name: 'Недоступно для студентов', value: BlockStatuses.HIDE, icon: 'lock' },
  { id: 2, name: 'Доступно для студентов', value: BlockStatuses.OPEN, icon: 'users' },
  { id: 3, name: 'На проверке', value: BlockStatuses.ON_CHECK, icon: 'rotate-ccw' },
  { id: 4, name: 'Проверено', value: BlockStatuses.CHECKED, icon: 'check-circle' },
  { id: 5, name: 'Архивировано', value: BlockStatuses.ARCHIVE, icon: 'archive' },
];

interface IBlockBase extends IData {
  id: number;
  title: string;
  content: string;
  course_id: number;
  is_team: boolean;
  is_project: boolean;
  is_exam: boolean;
  file: string | null;
  status: IBlockStatus | number | null;
  time_hide: string | null;
  time_open: string | null;
  order: number;
}

export interface IBlock extends IBlockBase {
  status: IBlockStatus | null;
}

export interface IBlockApi extends IBlockBase {
  status: number;
}

export enum BlockErrors {
  STATUS_ERROR = 'Status error',
  COURSE_ERROR = 'Course error',
}

const blockErrors: Record<BlockErrors, string> = {
  [BlockErrors.STATUS_ERROR]: 'Такой статус не существует',
  [BlockErrors.COURSE_ERROR]: 'Такой курс не существует',
};

export class BlockError extends ErrorBase<BlockErrors> {
  readonly data: string;

  constructor(name: BlockErrors, data: IBlockBase) {
    const message = blockErrors[name];
    super(name, message);
    this.data = JSON.stringify(data);
  }
}

export const defaultBlock: Partial<IBlock> = {
  is_exam: false,
  is_project: false,
  is_team: false,
  status: blockStatuses[0],
};
